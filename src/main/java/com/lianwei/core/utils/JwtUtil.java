package com.lianwei.core.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class JwtUtil {


    /**
     * 生成token
     *  * @param issuer    签发人
     *  * @param subject   代表这个JWT的主体，即它的所有人 一般是用户id
     *  * @param claims    存储在JWT里面的信息 一般放些用户的权限/角色信息
     */
    public static String generateToken(Map<String, Object> claims, String subject) {
        final Date createdDate = new Date();

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(createdDate)
                .signWith(SignatureAlgorithm.HS384, JwtConstants.SECRET)
                .compact();
    }

    /**
     * 获取jwt的payload部分(主体 | 有效载荷)
     */
    public static Claims getClaimFromToken(String token) {
        Claims claims = null;
        try{
            claims = Jwts.parser()
                    .setSigningKey(JwtConstants.SECRET)
                    .parseClaimsJws(token)
                    .getBody();
        }catch (ExpiredJwtException e){
            claims = e.getClaims();
        }
        return claims;
    }

    public static void main(String[] args) {
        String token = generateToken(new HashMap<String, Object>() {{
            put("parameter", 2001);
            //防止段时间内生成的token重复
            put("tokenId", UUID.randomUUID());
        }}, "111");
        System.out.println(token);
        System.out.println("自定义参数claim-------------------");
        //获取自定义参数
        Claims claimFromToken = getClaimFromToken(token);
        System.out.println(claimFromToken.get("parameter"));
        System.out.println(claimFromToken.get("tokenId"));
        System.out.println("主题subject--------------------");
        //获取主题subject
        String subject = claimFromToken.getSubject();
        System.out.println(subject);
    }

}
