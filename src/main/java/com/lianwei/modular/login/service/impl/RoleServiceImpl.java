package com.lianwei.modular.login.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lianwei.modular.login.domain.Role;
import com.lianwei.modular.login.service.RoleService;
import com.lianwei.modular.login.dao.RoleMapper;
import org.springframework.stereotype.Service;


/**
*
*/
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role>
implements RoleService{

}
