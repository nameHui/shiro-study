package com.lianwei.modular.login.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lianwei.modular.login.domain.User;


/**
*
*/
public interface UserService extends IService<User> {

    User selectByUserName(String username);
}
