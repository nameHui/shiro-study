package com.lianwei.modular.login.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lianwei.modular.login.domain.Permissions;


/**
*
*/
public interface PermissionsService extends IService<Permissions> {

}
