package com.lianwei.modular.login.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lianwei.modular.login.domain.User;
import com.lianwei.modular.login.service.UserService;
import com.lianwei.modular.login.dao.UserMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;

/**
*
*/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService{
    @Resource
    private UserMapper userMapper;

    @Override
    public User selectByUserName(String account) {
        return userMapper.selectByUserName(account);
    }
}
