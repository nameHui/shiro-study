package com.lianwei.modular.login.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lianwei.modular.login.domain.RolePermissions;


/**
*
*/
public interface RolePermissionsService extends IService<RolePermissions> {

}
