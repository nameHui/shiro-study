package com.lianwei.modular.login.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lianwei.modular.login.domain.Permissions;
import com.lianwei.modular.login.dao.PermissionsMapper;
import com.lianwei.modular.login.service.PermissionsService;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class PermissionsServiceImpl extends ServiceImpl<PermissionsMapper, Permissions> implements PermissionsService {

}
