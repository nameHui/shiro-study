package com.lianwei.modular.login.service;

import com.lianwei.modular.login.domain.User;

public interface DbRealmService {

    /**
     * 通过账号查询用户信息
     */
    User selectByUserName(String userName);

}
