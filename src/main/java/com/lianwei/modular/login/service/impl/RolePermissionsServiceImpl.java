package com.lianwei.modular.login.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lianwei.modular.login.domain.RolePermissions;
import com.lianwei.modular.login.service.RolePermissionsService;
import com.lianwei.modular.login.dao.RolePermissionsMapper;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class RolePermissionsServiceImpl extends ServiceImpl<RolePermissionsMapper, RolePermissions>
implements RolePermissionsService{

}
