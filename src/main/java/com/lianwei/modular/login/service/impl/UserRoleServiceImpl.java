package com.lianwei.modular.login.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lianwei.modular.login.domain.UserRole;
import com.lianwei.modular.login.service.UserRoleService;
import com.lianwei.modular.login.dao.UserRoleMapper;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService{

}
