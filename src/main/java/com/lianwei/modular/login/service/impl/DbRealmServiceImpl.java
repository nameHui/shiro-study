package com.lianwei.modular.login.service.impl;

//import com.lianwei.core.utils.MyApplicationContext;
import com.lianwei.modular.login.dao.UserMapper;
import com.lianwei.modular.login.domain.User;
import com.lianwei.modular.login.service.DbRealmService;
import jdk.jfr.Description;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;


/**
 * 由于shiro中的认证与授权实现类无法获取类的注入与装配，顾使用此类获取数据
 */
@Service
@DependsOn("userServiceImpl")

public class DbRealmServiceImpl implements DbRealmService {


    @Autowired
    private UserServiceImpl userMapper;

    @Override
    public User selectByUserName(String userName) {
        return userMapper.selectByUserName(userName);
    }
}
