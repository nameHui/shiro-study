package com.lianwei.modular.login.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lianwei.modular.login.domain.*;
import com.lianwei.modular.login.service.RolePermissionsService;
import com.lianwei.modular.login.service.UserRoleService;
import com.lianwei.modular.login.service.UserService;
import com.lianwei.modular.login.service.impl.DbRealmServiceImpl;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ShiroDbRealm extends AuthorizingRealm {
    @Autowired
    private UserService userService;
    @Resource
    private UserRoleService userRoleService;
    @Autowired
    private RolePermissionsService rolePermissionsService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private PermissionsService permissionsService;


    /**
     * 用户验证 调用login(token)时进这
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken)authenticationToken;
        String username = usernamePasswordToken.getUsername();
        //未加密的密码
        String password = new String(usernamePasswordToken.getPassword());
        User user = userService.selectByUserName(username);
        /**
         * 查出了返回对象，未找到用户返回null
         * 参数1 查出的用户信息
         * 参数2 数据库中的密码
         * 参数3 此类的class名
         */
        if(user==null || !user.getPassword().equals(new Sha512Hash(password, username, 12).toString())){
            throw new RuntimeException("用户不存在或密码错误");
        }
        return new SimpleAuthenticationInfo(user,usernamePasswordToken.getPassword(),ShiroDbRealm.class.getName());
    }

    /**
     * 权限验证(todo 每一次判断权限和判断角色都会调用一次这个方法，效率低)
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        if (principalCollection == null) {
            throw new AuthorizationException("Shiro中的权限校验参数不能为空");
        }
        //类型转换
        User user = (User) getAvailablePrincipal(principalCollection);
        //查询用户角色
        QueryWrapper<UserRole> userRole = new QueryWrapper<UserRole>().eq("user_id", user.getId());

        // todo 这里可以优化成一个sql查询效率更快
        List<UserRole> list = userRoleService.list(userRole);
        //角色集
        Set<String> roles = new HashSet<>();
        //权限集
        Set<String> permissions = new HashSet<>();
        //遍历所有角色获得他们的权限集
        for (UserRole userRole1 : list) {
            roles.add(roleService.getById(userRole1.getRoleId()).getRoleName());
            QueryWrapper<RolePermissions> rolePermissionsQuery = new QueryWrapper<RolePermissions>().eq("role_id", userRole1.getRoleId());
            List<RolePermissions> list1 = rolePermissionsService.list(rolePermissionsQuery);
            for (RolePermissions rolePermissions : list1) {
                Permissions permission = permissionsService.getById(rolePermissions.getPerId());
                permissions.add(permission.getPerUrl());
            }
        }
        System.out.println("角色：");
        System.out.println(roles);
        System.out.println("权限集：");
        System.out.println(permissions);
//        //授权信息
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
//        // 找到角色
        info.setRoles(roles);
//        //授予权限
        info.setStringPermissions(permissions);
        return info;
    }



}
