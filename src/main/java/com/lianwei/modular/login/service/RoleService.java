package com.lianwei.modular.login.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lianwei.modular.login.domain.Role;


/**
*
*/
public interface RoleService extends IService<Role> {

}
