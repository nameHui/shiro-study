package com.lianwei.modular.login.domain;

import java.io.Serializable;

/**
 * 
 * @TableName permissions
 */
public class Permissions implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 权限名
     */
    private String perName;

    /**
     * 权限路径
     */
    private String perUrl;

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     */
    public String getPerName() {
        return perName;
    }

    /**
     * 
     */
    public void setPerName(String perName) {
        this.perName = perName;
    }

    /**
     * 
     */
    public String getPerUrl() {
        return perUrl;
    }

    /**
     * 
     */
    public void setPerUrl(String perUrl) {
        this.perUrl = perUrl;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Permissions other = (Permissions) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getPerName() == null ? other.getPerName() == null : this.getPerName().equals(other.getPerName()))
            && (this.getPerUrl() == null ? other.getPerUrl() == null : this.getPerUrl().equals(other.getPerUrl()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getPerName() == null) ? 0 : getPerName().hashCode());
        result = prime * result + ((getPerUrl() == null) ? 0 : getPerUrl().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", perName=").append(perName);
        sb.append(", perUrl=").append(perUrl);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}