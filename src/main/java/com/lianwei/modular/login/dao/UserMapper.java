package com.lianwei.modular.login.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lianwei.modular.login.domain.User;


/**
* @Entity com.lianwei.modular.login.domain.User
*/
public interface UserMapper extends BaseMapper<User> {
    User selectByUserName(String username);
}
