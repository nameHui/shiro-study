package com.lianwei.modular.login.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lianwei.modular.login.domain.RolePermissions;


/**
* @Entity com.lianwei.modular.login.domain.RolePermissions
*/
public interface RolePermissionsMapper extends BaseMapper<RolePermissions> {


}
