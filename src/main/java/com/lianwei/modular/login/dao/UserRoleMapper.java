package com.lianwei.modular.login.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lianwei.modular.login.domain.UserRole;


/**
* @Entity com.lianwei.modular.login.domain.UserRole
*/
public interface UserRoleMapper extends BaseMapper<UserRole> {


}
