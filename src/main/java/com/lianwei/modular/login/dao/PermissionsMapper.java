package com.lianwei.modular.login.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lianwei.modular.login.domain.Permissions;


/**
* @Entity com.lianwei.modular.login.domain.Permissions
*/
public interface PermissionsMapper extends BaseMapper<Permissions> {


}
