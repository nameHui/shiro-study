package com.lianwei.modular.login.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lianwei.modular.login.domain.Role;


/**
* @Entity com.lianwei.modular.login.domain.Role
*/
public interface RoleMapper extends BaseMapper<Role> {


}
