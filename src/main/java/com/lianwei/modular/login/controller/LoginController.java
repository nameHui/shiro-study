package com.lianwei.modular.login.controller;

import com.lianwei.core.utils.JwtUtil;
import com.lianwei.core.utils.RedisUtil;
import com.lianwei.modular.login.domain.User;
import com.lianwei.modular.login.service.ShiroDbRealm;
import com.lianwei.modular.login.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.UUID;

/**
 * 登录器控制层
 */
@Controller
public class LoginController {

    @Autowired
    private UserService userService;
    @Autowired
    private RedisUtil redisUtil;

    @RequestMapping("/login")
    @ResponseBody
    public String login(@RequestParam String account,@RequestParam String password)  {
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
        defaultSecurityManager.setRealm(new ShiroDbRealm());
        SecurityUtils.setSecurityManager(defaultSecurityManager);
        //获取主体subject
        Subject subject = SecurityUtils.getSubject();
        boolean permitted0 = subject.isPermitted("/sellGoods");
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(account,password,false);
        //登录(认证)
        subject.login(usernamePasswordToken);
        boolean role = subject.hasRole("超级管理员");
        //是否有这个权限
        boolean permitted = subject.isPermitted("/sellGoods");
        HashMap<String, Object> param = new HashMap<>();
        User user = (User)subject.getPrincipals().getPrimaryPrincipal();
        String toKenId = UUID.randomUUID().toString();
        param.put("tokenId", toKenId);
        param.put("userId", user.getId());
        // 登录时间有效期为一小时
        redisUtil.setex(toKenId,user,60*60*60);
        String token = JwtUtil.generateToken(param, user.getId().toString());
        return token;
    }

    @RequestMapping("/register")
    @ResponseBody
    public String register(@RequestParam(required = true) String account, String password)  {
        User user = userService.selectByUserName(account);
        if(user != null){
            return "该用户已被注册";
        }
        user = new User();
        user.setUserName(account);
        String password1 = new Sha512Hash(password, account, 12).toString();
        user.setPassword(password1);
        userService.save(user);
        return "注册成功";
    }

    @RequestMapping("/noLogin")
    @ResponseBody
    public String noLogin(){
        return "你还没有登录哦，这里返回登录页面";
    }

    @RequestMapping("/noAuthorized")
    @ResponseBody
    public String noAuthorized(){
        return "你没有权限哦";
    }

    @RequestMapping("/index")
    @ResponseBody
    public String index(){
        return "这是首页";
    }


    @RequestMapping("/level1")
    @ResponseBody
    public String level1(){
        return "level1接口";
    }



}
