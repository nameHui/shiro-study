package com.lianwei.modular.login.controller;

import com.lianwei.modular.login.domain.User;
import com.lianwei.modular.login.service.PermissionsService;
import com.lianwei.modular.login.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ApiController {
    @Autowired
    private PermissionsService permissionsService;
    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping("/test")
    public String test(){
        User user = userService.selectByUserName("小明");
        System.out.println(user);
        return "true";
    }
}
