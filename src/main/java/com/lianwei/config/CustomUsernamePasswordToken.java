package com.lianwei.config;

import org.apache.shiro.authc.UsernamePasswordToken;

public class CustomUsernamePasswordToken extends UsernamePasswordToken {
    //定义一个token
    private String jwtToken;

    //返回token
    public CustomUsernamePasswordToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    //用户的身份信息是jwtToken（之前是数据库中的用户名）
    @Override
    public Object getPrincipal() {
        return jwtToken;
    }

    //用户的凭证信息是jwtToken（之前是数据库中的密码）
    @Override
    public Object getCredentials() {
        return jwtToken;
    }
}